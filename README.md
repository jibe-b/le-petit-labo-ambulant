# Le petit labo ambulant (une idée pour un autre nom ?)

## En quelques mots

Nous sommes quelques personnes réalisant des activités de recherche (parmi d'autres activités) et nous organisons un petit laboratoire ambulant (d'où le nom).

Quelques idées générales guident ce projet :
- choisir la frugalité pour s'éviter les complications des levées de fonds
- choisir la mobilité pour être proche de chacun des partenaires
- choisir la souveraineté et la gouvernance commune (une personne=une voix)
- faire vivre l'accompagnement par les pairs, l'apprentissage pair à pair afin de stimuler le dynamisme en chaque nœud du réseau

## Mise en place

Ce projet en est à ses premiers pas. Les forces vives sont les bienvenues, avec les objectifs :
- à court terme de fédérer quelques activités indépendantes sous le drapeau (non exclusif) de ce labo et booster quelques projets par son biais
- à moyen terme de mener des projets nés de ce labo
- à long terme de questionner la dynamique de ce laboratoire et envisager les itérations suivantes

## Contribuer

Ce type d'organisation peut vous convenir tout comme vous avez probablement des idées pour l'améliorer. Vos contributions sont les bienvenues !

Pour des idées, suggestions, etc, c'est dans le [gestionnaire d'issues](https://gitlab.com/jibe-b/le-petit-labo-ambulant/issues).

Si vous souhaitez partir avec ce projet pour vous en servir, cliquez sur le bouton "Fork" en haut à droite :)


